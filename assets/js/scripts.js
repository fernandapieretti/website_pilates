$(document).ready(function() {

    $(".ir-passo-2").click(function() {
        $("#dados1").removeClass().addClass('fadeOutLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            $("#dados1").removeClass().addClass("remove");
            $("#dados2").removeClass().addClass('fadeInRight animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $("#dados2").removeClass();
            });
        });
        $("#conteudo ul li").removeClass("active");
        $(".passo2").addClass("active");
		if ($(document).width() <= 768) {
			$("html, body").delay(2900).animate({scrollTop: $('#conteudo ul').offset().top }, 1000);
		}
    });

    $(".ir-passo-3").click(function() {
        $("#dados2").removeClass().addClass('fadeOutLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            $("#dados2").removeClass().addClass("remove");
            $("#dados3").removeClass().addClass('fadeInRight animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $("#dados3").removeClass();
            });
        });
        $("#conteudo ul li").removeClass("active");
        $(".passo3").addClass("active");
		if ($(document).width() <= 768) {
			$("html, body").delay(2900).animate({scrollTop: $('#conteudo ul').offset().top }, 1000);
		}
    });

    $(".voltar-passo-2").click(function() {
        $("#dados3").removeClass().addClass('fadeOutRight animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            $("#dados3").removeClass().addClass("remove");
            $("#dados2").removeClass().addClass('fadeInLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $("#dados2").removeClass();
            });
        });
        $("#conteudo ul li").removeClass("active");
        $(".passo2").addClass("active");
		if ($(document).width() <= 768) {
			$("html, body").delay(2900).animate({scrollTop: $('#conteudo ul').offset().top }, 1000);
		}
    });

    $(".voltar-passo-1").click(function() {
        $("#dados2").removeClass().addClass('fadeOutRight animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            $("#dados2").removeClass().addClass("remove");
            $("#dados1").removeClass().addClass('fadeInLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $("#dados1").removeClass();
            });
        });
        $("#conteudo ul li").removeClass("active");
        $(".passo1").addClass("active");
		if ($(document).width() <= 768) {
			$("html, body").delay(2900).animate({scrollTop: $('#conteudo ul').offset().top }, 1000);
		}
    });

    $("#datepicker").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro de', 'Fevereiro de', 'Março de', 'Abril de', 'Maio de', 'Junho de', 'Julho de', 'Agosto de', 'Setembro de', 'Outubro de', 'Novembro de', 'Dezembro de'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: '>',
        prevText: '<'
    });

});